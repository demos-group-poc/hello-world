# Hello World



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/demos-group-poc/hello-world.git
git branch -M main
git push -uf origin main
```

### first test pipeline using shared runners demo 

windows_job:
    tags:
        - shared-windows
    script:
        - echo "Windows OS Version"
        - systeminfo

linux_job:
    tags:
        - saas-linux-medium-amd64
    script:
        - echo "Linux OS Version"
        - cat /etc/os-release

macos_job:
    tags:
        - saas-macos-medium-m1
    script:
        - echo "MacOS OS Version"
        - system_profiler SPSoftwareDataType


### Second test pipeline using before_install & after_install to install the 3rd party softwares such as cowsay to print the dragon text

 workflow:
  name: Generate ASCII Artwork
ascii_job:
  before_script:
    - gem install cowsay
  script:
    - echo "Generating ASCII Artwork using COWSAY Program"
    - cowsay -f dragon "Run for cover, I am a Dragon.... RAWR" >> dragon.txt
    - grep -i "dragon" dragon.txt
    - cat dragon.txt
  after_script:
    - echo "Executed at the end, can be used for cleaning/removing content"


#### Executing shell script in the pipeline job as below - 3rd scenairo and create the above list of commands into a shell script 
#### ascii_script.sh and provide the execute permission in before_script which will perform the same steps as above 

workflow:
  name: Generate ASCII Artwork

ascii_job:
  before_script:
    - gem install cowsay
    - chmod +x ascii-script.sh
  script:
    - ./ascii-script.sh
  after_script:
    - echo "Executed at the end, can be used for cleaning/removing content"


### Pipeline with Multiple Depedencies can be get failed  as there are no stages to define the order of execution in the below workflow usecase

workflow:
  name: Generate ASCII Artwork

build_job_1:
  before_script:
    - gem install cowsay
    - sleep 30s
  script:
    - >
       cowsay -f dragon "Run for cover,
       I am a DRAGON...RAWR" >> dragon.txt

test_job_2:
  script:
    - |
       sleep 10s
       grep -i "dragon" dragon.txt

deploy_job_3:
  script:
    - cat dragon.txt
    - echo "deploying ......."


### Pipeline with Stage and Stages which helps in execution of the jobs in an orderly fashion - one after the other
### The Pipeline still fails as the build job - execution should have to save the result into the artifacts which should 
### be consumed by the later stages for which artifacts are being required

workflow:
  name: Generate ASCII Artwork

stages:
  - build
  - test
  - deploy

build_job_1:
  stage: build
  before_script:
    - gem install cowsay
    - sleep 30s
  script:
    - >
       cowsay -f dragon "Run for cover,
       I am a DRAGON...RAWR" >> dragon.txt

test_job_2:
  stage: test
  script:
    - |
       sleep 10s
       grep -i "dragon" dragon.txt

deploy_job_3:
  stage: deploy
  script:
    - cat dragon.txt
    - echo "deploying ......."

#### Artifacts - Using artifacts is very much essential as the build artifacts in 1st stage is necessary for the following next stages . In this case the testjob 2 will consume the artifacts from build job 1 and will proceed with its job execution - as in the test job 2 i.e in stage test its looking for dragon string in dragon.txt file - which it will extract from the build job 1 artifacts section.

workflow:
  name: Generate ASCII Artwork

stages:
  - build
  - test
  - deploy

build_job_1:
  stage: build
  before_script:
    - gem install cowsay
    - sleep 30s
  script:
    - >
       cowsay -f dragon "Run for cover,
       I am a DRAGON...RAWR" >> dragon.txt
  artifacts:
    name: Dragon Text File 
    paths:
      - dragon.txt
    when: on_success
    access: all
    expire_in: 1 day

test_job_2:
  stage: test
  script:
    - |
       sleep 10s
       grep -i "dragon" dragon.txt

deploy_job_3:
  stage: deploy
  script:
    - cat dragon.txt
    - echo "deploying ......."


### Needs -  are usefull to execute the jobs in a sequential manner 


workflow:
  name: Generate ASCII Artwork

stages:
  - build
  - test
  - docker
  - deploy

build_file:
  stage: build
  before_script:
    - gem install cowsay
    - sleep 30s
  script:
    - >
       cowsay -f dragon "Run for cover,
       I am a DRAGON...RAWR" >> dragon.txt
  artifacts:
    name: Dragon Text File 
    paths:
      - dragon.txt
    when: on_success
    access: all
    expire_in: 1 day

test_file:
  stage: test
  script:
    - |
       sleep 10s
       grep -i "dragon" dragon.txt

docker_build:
  stage: docker
  script:
    - echo "docker build -t docker.io/dockerUsername/imageName:version"
    - sleep 15s

docker_testing:
  stage: docker
  script:
    - echo "docker run -p 80:80 docker.io/dockerUsername/imageName:version"
    - exit 1

docker_push:
  stage: docker
  script:
    - echo "docker login --username=dockerUsername --password=s3cUrePaSsw0rd"
    - echo "docker push docker.io/dockerUsername/imageName:Version"


deploy_ec2:
  stage: deploy
  script:
    - cat dragon.txt
    - echo "deploying ......."


### Passing variables at Global Level 

workflow:
  name: Generate ASCII Artwork

stages:
  - build
  - test
  - docker
  - deploy

variables:
  USERNAME: dockerUsername
  REGISTRY: docker.io/$USERNAME
  IMAGE: imageName
  VERSION: $CI_PIPELINE_ID

build_file:
  stage: build
  before_script:
    - gem install cowsay
    - sleep 30s
  script:
    - >
       cowsay -f dragon "Run for cover,
       I am a DRAGON...RAWR" >> dragon.txt
  artifacts:
    name: Dragon Text File 
    paths:
      - dragon.txt
    when: on_success
    access: all
    expire_in: 1 day

test_file:
  stage: test
  script:
    - |
       sleep 10s
       grep -i "dragon" dragon.txt

docker_build:
  stage: docker
  needs:
    - build_file
  script:
    - echo "docker build -t $REGISTRY/$IMAGE:$VERSION"

docker_testing:
  stage: docker
  needs:
    - docker_build
  script:
    - echo "docker run -p 80:80 $REGISTRY/$IMAGE:$VERSION"
    - exit 1

docker_push:
  stage: docker
  needs:
    - docker_testing
  variables:
    - PASSWORD: s3cUrePaSsw0rd
  script:
    - echo "docker login --username=dockerUsername --password=s3cUrePaSsw0rd"
    - echo "docker push docker.io/dockerUsername/imageName:Version"


deploy_ec2:
  stage: deploy
  script:
    - cat dragon.txt
    - echo "deploying ......."
    - echo "Username - $USERNAME and password - $PASSWORD"




